﻿Option Explicit On
Option Strict On

Imports System.IO

Public Class FindCustomer

    Dim lsDataBooking As List(Of Hashtable)
    Dim lsDataCustomer As List(Of Hashtable)
    Dim lsDataRoom As List(Of Hashtable)
    Dim lsAll As List(Of Hashtable)
    Dim keyword As String
    Dim CustomerID As Integer

    Private Sub FindBooking_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim oController As DataController = New DataController
        'set find options
        cbbFind.Items.Add("customer Name")
        cbbFind.Items.Add("customer ID")
        'load datagridview
        loadDatabase()

        If dgvSearchResult.Rows.Count > 1 Then fillform(0)
    End Sub

    Private Sub cbbFind_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbbFind.SelectedIndexChanged
        Dim selectedIndex As Integer = cbbFind.SelectedIndex
        txtfindcustomervalue.Enabled = True
    End Sub


    Private Function validateinput() As Boolean
        Dim oValidation As New Validation
        Dim tt As New ToolTip()
        Dim bIsValid As Boolean = True

        If cbbFind.SelectedText = "customer Name" Then
            bIsValid = oValidation.istext(txtfindcustomervalue.Text)
            If bIsValid Then
                picErrorField.Visible = False

            Else
                picErrorField.Visible = True
                tt.SetToolTip(picErrorField, "Please choose a search option and enter a right format")

            End If
        End If

        If cbbFind.SelectedText = "customer ID" Then
            bIsValid = oValidation.IsNumericVal(txtfindcustomervalue.Text)
            If bIsValid Then
                picErrorField.Visible = False

            Else
                picErrorField.Visible = True
                tt.SetToolTip(picErrorField, "Please choose a search option and enter a right format")

            End If
        End If
        Return False
    End Function

    'load database on datagrid
    Private Sub loadDatabase()
        Dim oController As DataController = New DataController
        'pull data out of database
        Dim tables As DataTableCollection
        tables = oController.getDatacustomer(keyword)
        Dim view As New DataView(tables(0))
        Dim source1 As New BindingSource
        source1.DataSource = view
        dgvSearchResult.DataSource = view
    End Sub

    Private Sub btnfindcustomer_Click(sender As Object, e As EventArgs) Handles btnfindcustomer.Click
        'validate input
        Dim tt As New ToolTip()
        If Not validateinput() Then
            picErrorField.Visible = True
            tt.SetToolTip(picErrorField, "Please choose a search option and enter a right format")
            Exit Sub

        End If

        keyword = txtfindcustomervalue.Text

        'clear everyting in data grid view for the result
        dgvSearchResult.DataSource = Nothing
        dgvSearchResult.Refresh()
        loadDatabase()

    End Sub

    'table columm order:
    'customer id 0
    'title 1
    'first name 2
    'last name 3
    'phone 4
    'address 5 
    'email 6
    'dob 7 
    'gender 8

    Private Sub fillform(ByVal i As Integer)
        CustomerID = Convert.ToInt16(dgvSearchResult.Item(0, i).Value)
        txtcustomerid.Text = dgvSearchResult.Item(0, i).Value.ToString
        txtFirstName.Text = dgvSearchResult.Item(2, i).Value.ToString
        txtLastName.Text = dgvSearchResult.Item(3, i).Value.ToString
        If dgvSearchResult.Item(8, i).Value.ToString = "Male" Then
            radiobtnMale.Checked = True
        Else
            radiobtnFemale.Checked = True
        End If

        txtPhone.Text = dgvSearchResult.Item(4, i).Value.ToString
        txtAddress.Text = dgvSearchResult.Item(5, i).Value.ToString
        txtEmail.Text = dgvSearchResult.Item(6, i).Value.ToString
        txtdob.Text = dgvSearchResult.Item(7, i).Value.ToString
        txttitle.Text = dgvSearchResult.Item(1, i).Value.ToString
    End Sub

    'when click on a row, fill form
    Private Sub dgvSearchResult_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvSearchResult.CellClick
        Dim i As Integer
        i = dgvSearchResult.CurrentRow.Index
        If dgvSearchResult.Item(0, i).Value.ToString Is "" Then Exit Sub
        fillform(i)
    End Sub

    'validate
    Private Function validateFormData() As Boolean
        Dim oValidation As New Validation
        Dim bIsValid As Boolean
        Dim bAllFieldsValid As Boolean
        bAllFieldsValid = True
        Dim tt As New ToolTip()

        ' Check number of days is number
        'Validate gender
        If radiobtnMale.Checked = False And radiobtnFemale.Checked = False Then
            picErrorField1.Visible = True
            tt.SetToolTip(picErrorField1, "Please choose your gender")
            bAllFieldsValid = False
        Else picErrorField1.Visible = False
        End If

        'Validate first name - only text
        bIsValid = oValidation.istext(txtFirstName.Text)
        If bIsValid Then
            picErrorField2.Visible = False
        Else
            picErrorField2.Visible = True
            tt.SetToolTip(picErrorField2, "First name is empty or contain numbers")
            bAllFieldsValid = False
        End If

        'Validate last name - only text
        bIsValid = oValidation.istext(txtLastName.Text)
        If bIsValid Then
            picErrorField3.Visible = False
        Else
            picErrorField3.Visible = True
            tt.SetToolTip(picErrorField3, "Last name is empty or contain numbers")
            bAllFieldsValid = False
        End If

        'Validate phone number - only number        
        bIsValid = IsNumeric(txtPhone.Text)
        If bIsValid Then
            picErrorField4.Visible = False
        Else
            picErrorField4.Visible = True
            tt.SetToolTip(picErrorField4, "Characters and blank space are not allow")
            bAllFieldsValid = False
        End If

        'Validate address - no empty
        bIsValid = oValidation.IsAlphaNumeric(txtAddress.Text)
        If bIsValid Then
            picErrorField5.Visible = False
        Else
            picErrorField5.Visible = True
            tt.SetToolTip(picErrorField5, "Please enter your address")
            bAllFieldsValid = False
        End If

        'Validate email - email format only
        bIsValid = oValidation.isvalidateEmail(txtEmail.Text)
        If bIsValid Then
            picErrorField6.Visible = False
        Else
            picErrorField6.Visible = True
            tt.SetToolTip(picErrorField6, "Please enter correct email format")
            bAllFieldsValid = False
        End If

        'check all field
        If bAllFieldsValid Then
            Debug.Print("All fields are valid")
        End If
        Return bAllFieldsValid

    End Function

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        Dim bIsValid = validateFormData()

        If btnEdit.Text = "Edit" Then
            txtdob.Visible = False
            DobValue.Visible = True
            txtFirstName.Enabled = True
            txtLastName.Enabled = True
            txtPhone.Enabled = True
            txtAddress.Enabled = True
            txtEmail.Enabled = True
            radiobtnFemale.Enabled = True
            radiobtnMale.Enabled = True
            btnEdit.Text = "Save Edit"
            Exit Sub
        Else
            txtdob.Visible = True
            DobValue.Visible = False
            txtFirstName.Enabled = False
                txtLastName.Enabled = False
                txtPhone.Enabled = False
                txtAddress.Enabled = False
                txtEmail.Enabled = False
                radiobtnFemale.Enabled = False
            radiobtnMale.Enabled = False
            btnEdit.Text = "Edit"
            'Update the record
            ' Instantiate a hashtable and populate it with form data

            If bIsValid Then
                Dim htData As Hashtable = New Hashtable

                If radiobtnMale.Checked Then
                    htData("title") = "Mr"
                ElseIf radiobtnFemale.Checked Then
                    htData("title") = "Ms"
                End If
                htData("firstname") = txtFirstName.Text
                htData("lastname") = txtLastName.Text
                htData("phone") = txtPhone.Text
                htData("address") = txtAddress.Text
                htData("email") = txtEmail.Text
                htData("dob") = DobValue.Value
                If radiobtnMale.Checked Then
                    htData("gender") = "Male"
                Else
                    htData("gender") = "Female"
                End If
                htData("customer_id") = CustomerID

                Dim oDataController As DataController = New DataController
                Dim iNumRows = oDataController.updatecustomer(htData)
                If iNumRows = 1 Then
                    MsgBox("The booking information is updated.")
                    loadDatabase()
                Else
                    MsgBox("The booking information is NOT updated.")
                End If
            End If
        End If
    End Sub

    Private Sub btntDelete_Click_1(sender As Object, e As EventArgs) Handles btntDelete.Click
        Dim oDataController As DataController = New DataController
        Dim iNumRows = oDataController.deletecustomer(CStr(CustomerID))

        If iNumRows = 1 Then
            MsgBox("The booking information is Deleted.")
            loadDatabase()
        Else
            MsgBox("The booking information is NOT Deleted. ")
        End If
    End Sub

    '?
    Private Sub btntDelete_Click(sender As Object, e As EventArgs)
        txtFirstName.Text = dgvSearchResult.Rows(2).Cells(2).ToString
    End Sub

    Private Sub btnClose_Click_1(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnNext_Click_1(sender As Object, e As EventArgs) Handles btnNext.Click
        If dgvSearchResult.CurrentRow.Index < dgvSearchResult.RowCount - 2 Then
            dgvSearchResult.CurrentCell = dgvSearchResult.Item(0, dgvSearchResult.CurrentRow.Index + 1)
            fillform(dgvSearchResult.CurrentRow.Index)
        End If
    End Sub

    Private Sub btnPrev_Click_1(sender As Object, e As EventArgs) Handles btnPrev.Click
        If dgvSearchResult.CurrentRow.Index > 0 Then
            dgvSearchResult.CurrentCell = dgvSearchResult.Item(0, dgvSearchResult.CurrentRow.Index - 1)
            fillform(dgvSearchResult.CurrentRow.Index)
        End If
    End Sub

    Private Sub btnreport_Click(sender As Object, e As EventArgs) Handles btnreport.Click
        Dim oDataController As DataController = New DataController
        oDataController.createBreakReport()
    End Sub
End Class