﻿Option Explicit On
Option Strict On

Imports System.IO

Public Class FindRoom
    Dim lsDataBooking As List(Of Hashtable)
    Dim lsDataCustomer As List(Of Hashtable)
    Dim lsDataRoom As List(Of Hashtable)
    Dim lsAll As List(Of Hashtable)
    Dim keyword As String
    Dim RoomID As Integer

    Private Sub FindRoom_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim oController As DataController = New DataController
        'set find options
        cbbFind.Items.Add("Room Number")
        cbbFind.Items.Add("Room type")
        'load datagridview
        loadDatabase()

        If dgvSearchResult.Rows.Count > 1 Then fillform(0)
    End Sub

    Private Sub cbbFind_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbbFind.SelectedIndexChanged
        Dim selectedIndex As Integer = cbbFind.SelectedIndex
        txtfindroomvalue.Enabled = True
    End Sub

    Private Function validateinput() As Boolean
        Dim oValidation As New Validation
        Dim tt As New ToolTip()
        Dim bIsValid As Boolean = True

        bIsValid = oValidation.istext(txtfindroomvalue.Text)
        If bIsValid Then
            picErrorField.Visible = False
            Return bIsValid
        Else
            picErrorField.Visible = True
            tt.SetToolTip(picErrorField, "Please choose a search option and enter a right format")
            Return False
        End If
    End Function

    Private Sub loadDatabase()
        Dim oController As DataController = New DataController
        'pull data out of database
        Dim tables As DataTableCollection
        tables = oController.getDataRoom(keyword)
        Dim view As New DataView(tables(0))
        Dim source1 As New BindingSource
        source1.DataSource = view
        dgvSearchResult.DataSource = view
    End Sub

    Private Sub btnfindroom_Click(sender As Object, e As EventArgs) Handles btnfindroom.Click
        'validate input
        Dim tt As New ToolTip()
        If Not validateinput() Then
            picErrorField.Visible = True
            tt.SetToolTip(picErrorField, "Please choose a search option and enter a right format")
            Exit Sub
        End If

        keyword = txtfindroomvalue.Text

        'clear everyting in data grid view for the result
        dgvSearchResult.DataSource = Nothing
        dgvSearchResult.Refresh()
        loadDatabase()
    End Sub

    'table columm order:
    'room id 0
    'room_number 1
    'type 2
    'price 3
    'num_bed 4
    'availability 5 
    'floor 6
    'description 7 

    Private Sub fillform(ByVal i As Integer)
        RoomID = Convert.ToInt16(dgvSearchResult.Item(0, i).Value)
        txtRoomNumber.Text = dgvSearchResult.Item(1, i).Value.ToString
        txtType.Text = dgvSearchResult.Item(2, i).Value.ToString
        txtPrice.Text = dgvSearchResult.Item(3, i).Value.ToString
        txtNumberOfBeds.Text = dgvSearchResult.Item(4, i).Value.ToString
        txtFloor.Text = dgvSearchResult.Item(6, i).Value.ToString
        txtDescription.Text = dgvSearchResult.Item(7, i).Value.ToString
        If dgvSearchResult.Item(5, i).Value.ToString = "True" Then
            radiobtnAvailable.Checked = True
        Else
            radiobtnUnavailable.Checked = True
        End If
    End Sub

    'when click on a row, fill form
    Private Sub dgvSearchResult_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvSearchResult.CellClick
        Dim i As Integer
        i = dgvSearchResult.CurrentRow.Index
        If dgvSearchResult.Item(0, i).Value.ToString Is "" Then Exit Sub
        fillform(i)
    End Sub

    'Check validation
    Private Function validateFormData() As Boolean
        Dim tt As New ToolTip()
        Dim oValidation As New Validation
        Dim bIsValid As Boolean
        Dim bAllFieldsValid As Boolean = True

        'Check Room Number
        bIsValid = IsNumeric(txtRoomNumber.Text)
        If bIsValid Then
            picErrorField1.Visible = False
        Else
            picErrorField1.Visible = True
            tt.SetToolTip(picErrorField1, "Please enter room number")
            bAllFieldsValid = False
        End If

        'Check room type
        bIsValid = oValidation.istext(txtType.Text)
        If bIsValid Then
            picErrorField2.Visible = False
        Else
            picErrorField2.Visible = True
            tt.SetToolTip(picErrorField2, "Room type is text only")
            bAllFieldsValid = False
        End If

        'Check price
        bIsValid = IsNumeric(txtPrice.Text)
        If bIsValid Then
            picErrorField3.Visible = False
        Else
            picErrorField3.Visible = True
            tt.SetToolTip(picErrorField3, "Please enter numerical price")
            bAllFieldsValid = False
        End If

        'check number of bed
        bIsValid = IsNumeric(txtNumberOfBeds.Text)
        If bIsValid Then
            picErrorField4.Visible = False
        Else
            picErrorField4.Visible = True
            tt.SetToolTip(picErrorField4, "Please enter number of bed")
            bAllFieldsValid = False
        End If

        'check Availability
        If radiobtnAvailable.Checked = False And radiobtnUnavailable.Checked = False Then
            picErrorField5.Visible = True
            tt.SetToolTip(picErrorField5, "Please choose availability")
            bAllFieldsValid = False
        Else picErrorField5.Visible = False
        End If

        'check floor
        bIsValid = IsNumeric(txtFloor.Text)
        If bIsValid Then
            picErrorField6.Visible = False
        Else
            picErrorField6.Visible = True
            tt.SetToolTip(picErrorField6, "Please enter floor number")
            bAllFieldsValid = False
        End If

        'check description
        bIsValid = oValidation.IsAlphaNumeric(txtDescription.Text)
        If bIsValid Then
            picErrorField7.Visible = False
        Else
            picErrorField7.Visible = True
            tt.SetToolTip(picErrorField7, "Please enter description")
            bAllFieldsValid = False
        End If

        'check all field
        If bAllFieldsValid Then
            Debug.Print("All fields are valid")
        End If
        Return bAllFieldsValid
    End Function

    'edit/update
    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        Dim bIsValid = validateFormData()
        If btnEdit.Text = "Edit" Then
            radiobtnAvailable.Enabled = True
            radiobtnUnavailable.Enabled = True
            txtRoomNumber.Enabled = True
            txtType.Enabled = True
            txtPrice.Enabled = True
            txtNumberOfBeds.Enabled = True
            txtFloor.Enabled = True
            txtDescription.Enabled = True
            btnEdit.Text = "Save Edit"
            Exit Sub
        Else
            radiobtnAvailable.Enabled = False
            radiobtnUnavailable.Enabled = False
            txtRoomNumber.Enabled = False
            txtType.Enabled = False
            txtPrice.Enabled = False
            txtNumberOfBeds.Enabled = False
            txtFloor.Enabled = False
            txtDescription.Enabled = False
            btnEdit.Text = "Edit"
            'Update the record
            ' Instantiate a hashtable and populate it with form data

            If bIsValid Then
                Dim htData As Hashtable = New Hashtable

                'take data from textbox
                htData("room_number") = txtRoomNumber.Text
                htData("type") = txtType.Text
                htData("price") = txtPrice.Text
                htData("num_bed") = txtNumberOfBeds.Text
                htData("floor") = txtFloor.Text
                htData("description") = txtDescription.Text
                If radiobtnAvailable.Checked = True Then
                    htData("availability") = True
                ElseIf radiobtnUnavailable.Checked = True Then
                    htData("availability") = False
                End If
                htData("room_id") = RoomID

                Dim oDataController As DataController = New DataController
                Dim iNumRows = oDataController.updateroom(htData)
                If iNumRows = 1 Then
                    MsgBox("The booking information is updated.")
                    loadDatabase()
                Else
                    MsgBox("The booking information is NOT updated.")
                End If
            End If
        End If
    End Sub

    Private Sub btntDelete_Click(sender As Object, e As EventArgs) Handles btntDelete.Click
        Dim oDataController As DataController = New DataController
        Dim iNumRows = oDataController.deleteroom(CStr(RoomID))

        If iNumRows = 1 Then
            MsgBox("The booking information is Deleted.")
            loadDatabase()
        Else
            MsgBox("The booking information is NOT Deleted. ")
        End If
    End Sub

    '?
    'Private Sub btntDelete_Click(sender As Object, e As EventArgs) Handles btntDelete.Click
    '   txtRoomNumber.Text = dgvSearchResult.Rows(2).Cells(2).ToString
    ' End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnNext_Click(sender As Object, e As EventArgs) Handles btnNext.Click
        If dgvSearchResult.CurrentRow.Index < dgvSearchResult.RowCount - 2 Then
            dgvSearchResult.CurrentCell = dgvSearchResult.Item(0, dgvSearchResult.CurrentRow.Index + 1)
            fillform(dgvSearchResult.CurrentRow.Index)
        End If
    End Sub

    Private Sub btnPrev_Click(sender As Object, e As EventArgs) Handles btnPrev.Click
        If dgvSearchResult.CurrentRow.Index > 0 Then
            dgvSearchResult.CurrentCell = dgvSearchResult.Item(0, dgvSearchResult.CurrentRow.Index - 1)
            fillform(dgvSearchResult.CurrentRow.Index)
        End If
    End Sub

End Class