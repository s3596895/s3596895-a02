﻿Option Explicit On
Option Strict On

Imports System.IO

Public Class FindBooking

    Dim lsDataBooking As List(Of Hashtable)
    Dim lsDataCustomer As List(Of Hashtable)
    Dim lsDataRoom As List(Of Hashtable)
    Dim lsAll As List(Of Hashtable)
    Dim keyword As String
    Dim bookingID As Integer

    Private Sub FindBooking_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim oController As DataController = New DataController
        'set find options
        cbbFind.Items.Add("Customer Name")

        'load datagridview
        loadDatabase()

        If dgvSearchResult.Rows.Count > 1 Then fillform(0)

    End Sub

    Private Sub cbbFind_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbbFind.SelectedIndexChanged
        Dim selectedIndex As Integer = cbbFind.SelectedIndex
        txtfindbookingvalue.Enabled = True
    End Sub

    Private Function validateinput() As Boolean
        Dim oValidation As New Validation
        Dim tt As New ToolTip()
        Dim bIsValid As Boolean = True

        bIsValid = oValidation.istext(txtfindbookingvalue.Text)
        If bIsValid Then
            picErrorField.Visible = False
            Return bIsValid
        Else
            picErrorField.Visible = True
            tt.SetToolTip(picErrorField, "Please choose a search option and enter a right format")
            Return False
        End If
    End Function

    'load database into datagrid
    Private Sub loadDatabase()
        Dim oController As DataController = New DataController
        'pull data out of database
        Dim tables As DataTableCollection
        tables = oController.getDataBooking(keyword)
        Dim view As New DataView(tables(0))
        Dim source1 As New BindingSource
        source1.DataSource = view
        dgvSearchResult.DataSource = view
    End Sub

    Private Sub btnfindbooking_Click(sender As Object, e As EventArgs) Handles btnfindbooking.Click
        'validate input
        Dim tt As New ToolTip()
        If Not validateinput() Then
            picErrorField.Visible = True
            tt.SetToolTip(picErrorField, "Please choose a search option and enter a right format")
            Exit Sub

        End If

        keyword = txtfindbookingvalue.Text

        'clear everyting in data grid view for the result
        dgvSearchResult.DataSource = Nothing
        dgvSearchResult.Refresh()
        loadDatabase()

    End Sub

    'table columm order:
    'booking id 0
    'booking date 1
    'number of days 2
    'guest 3
    'checkin date 4
    'comment 5 
    'customer id 6
    'room id 7 
    'Total price 8
    'tittle 9
    'firstname 10
    'lastname 11
    'phone 12
    'address 13
    'email 14
    'dob 15 
    'gender 16
    'room id 17
    'room number 18
    'type 19
    'price 20
    'num bed 21
    'availability 22
    'floor 23
    'description 23

    Private Sub fillform(ByVal i As Integer)
        bookingID = Convert.ToInt16(dgvSearchResult.Item(0, i).Value)
        txtCustomerName.Text = dgvSearchResult.Item(10, i).Value.ToString & " " & dgvSearchResult.Item(11, i).Value.ToString
        txtCheckinDate.Text = dgvSearchResult.Item(4, i).Value.ToString

        cbbCustomerID.Text = dgvSearchResult.Item(6, i).Value.ToString
        txtComment.Text = dgvSearchResult.Item(5, i).Value.ToString
        txtNumberDays.Text = dgvSearchResult.Item(2, i).Value.ToString
        txtNumberGuests.Text = dgvSearchResult.Item(3, i).Value.ToString
        cbbRoomID.Text = dgvSearchResult.Item(7, i).Value.ToString
        txtRoomPrice.Text = dgvSearchResult.Item(22, i).Value.ToString
        txtRoomNumber.Text = dgvSearchResult.Item(18, i).Value.ToString
        txtRoomType.Text = dgvSearchResult.Item(19, i).Value.ToString
        CheckinDate.Value = Convert.ToDateTime(dgvSearchResult.Item(4, i).Value)
        txtTotalPrice.Text = dgvSearchResult.Item(8, i).Value.ToString
    End Sub

    'when click on a row, fill form
    Private Sub dgvSearchResult_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvSearchResult.CellClick
        Dim i As Integer
        i = dgvSearchResult.CurrentRow.Index
        If dgvSearchResult.Item(0, i).Value.ToString Is "" Then Exit Sub
        fillform(i)
    End Sub

    'validate
    Private Function validateFormData() As Boolean
        Dim oValidation As New Validation
        Dim bIsValid As Boolean
        Dim bAllFieldsValid As Boolean
        bAllFieldsValid = True
        Dim tt As New ToolTip()

        ' Check number of days is number
        bIsValid = IsNumeric(txtNumberDays.Text)
        If bIsValid Then
            PictureBox3.Visible = False
        Else
            PictureBox3.Visible = True
            tt.SetToolTip(PictureBox3, "Number of days must be a number")
            bAllFieldsValid = False
        End If

        ' Check number of guests is number
        bIsValid = IsNumeric(txtNumberGuests.Text)
        If bIsValid Then
            PictureBox4.Visible = False
        Else
            PictureBox4.Visible = True
            tt.SetToolTip(PictureBox4, "Number of guests must be a number")
            bAllFieldsValid = False
        End If

        ' Check total price not empty
        If txtTotalPrice.Text IsNot "" Then
            PictureBox5.Visible = False
        Else
            PictureBox5.Visible = True
            tt.SetToolTip(PictureBox5, "Please calculate the total price")
            bAllFieldsValid = False
        End If

        ' Check comment not empty
        If txtComment.Text IsNot "" Then
            PictureBox6.Visible = False
        Else
            PictureBox6.Visible = True
            tt.SetToolTip(PictureBox6, "Please choose a room")
            bAllFieldsValid = False
        End If

        'Validate all field
        If bAllFieldsValid Then
            Debug.Print("All fields are valid")
        End If
        Return bAllFieldsValid

    End Function

    Private Sub btncal_Click(sender As Object, e As EventArgs) Handles btncal.Click
        If txtRoomPrice.Text IsNot "" And txtNumberDays.Text IsNot "" Then txtTotalPrice.Text = CStr(Convert.ToInt64(txtRoomPrice.Text) * Convert.ToInt64(txtNumberDays.Text))
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        Dim bIsValid = validateFormData()
        If btnEdit.Text = "Edit" Then
            txtCheckinDate.Visible = False
            CheckinDate.Visible = True
            txtComment.Enabled = True
            txtNumberDays.Enabled = True
            txtNumberGuests.Enabled = True
            btnEdit.Text = "Save Edit"
            Exit Sub
        Else txtCheckinDate.Visible = True
            CheckinDate.Visible = False
            txtComment.Enabled = False
            txtNumberDays.Enabled = False
            txtNumberGuests.Enabled = False
            btnEdit.Text = "Edit"

            'Update the record
            ' Instantiate a hashtable and populate it with form data

            If bIsValid Then
                Dim htData As Hashtable = New Hashtable

                htData("checkin_date") = CheckinDate.Value
                htData("num_days") = txtNumberDays.Text
                htData("num_guests") = txtNumberGuests.Text
                htData("comments") = txtComment.Text
                htData("booking_id") = bookingID

                Dim oDataController As DataController = New DataController
                Dim iNumRows = oDataController.updatebooking(htData)
                If iNumRows = 1 Then
                    MsgBox("The booking information is updated.")
                    loadDatabase()
                Else
                    MsgBox("The booking information is NOT updated.")
                End If
            End If
        End If
    End Sub

    Private Sub btntDelete_Click_1(sender As Object, e As EventArgs) Handles btntDelete.Click
        Dim oDataController As DataController = New DataController
        Dim iNumRows = oDataController.deleteBooking(CStr(bookingID))
        If iNumRows = 1 Then
            MsgBox("The booking information is Deleted.")
            loadDatabase()
        Else
            MsgBox("The booking information is NOT Deleted.")
        End If
    End Sub

    '?
    Private Sub btntDelete_Click(sender As Object, e As EventArgs)
        txtCustomerName.Text = dgvSearchResult.Rows(2).Cells(2).ToString
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs)
        Me.Close()
    End Sub

    Private Sub btnClose_Click_1(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnPrev_Click(sender As Object, e As EventArgs) Handles btnPrev.Click
        If dgvSearchResult.CurrentRow.Index > 0 Then
            dgvSearchResult.CurrentCell = dgvSearchResult.Item(0, dgvSearchResult.CurrentRow.Index - 1)
            fillform(dgvSearchResult.CurrentRow.Index)
        End If
    End Sub

    Private Sub btnNext_Click(sender As Object, e As EventArgs) Handles btnNext.Click
        If dgvSearchResult.CurrentRow.Index < dgvSearchResult.RowCount - 2 Then
            dgvSearchResult.CurrentCell = dgvSearchResult.Item(0, dgvSearchResult.CurrentRow.Index + 1)
            fillform(dgvSearchResult.CurrentRow.Index)
        End If
    End Sub

    Private Sub btnreport_Click(sender As Object, e As EventArgs) Handles btnreport.Click
        Dim oDataController As DataController = New DataController
        oDataController.createBreakReport()
    End Sub
End Class