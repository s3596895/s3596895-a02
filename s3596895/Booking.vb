﻿Option Strict On
Option Explicit On
'Name: Mainmenu.vb
'Description: Form for booking (attempt number 2)
'Author: Dao Ngoc Minh s3596895
'Date: 21/11/2017
'This code is written by Dao Ngoc Minh s3596895
Imports System.IO

Public Class Booking
    Dim lsDataRoom As List(Of Hashtable)
    Dim lsDataCustomer As List(Of Hashtable)

    Private Sub Booking2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'show item in drop down list
        cbbRoomID.DropDownStyle = ComboBoxStyle.DropDownList
        Dim oController As DataController = New DataController
        lsDataRoom = oController.findAll()

        For Each Room In lsDataRoom
            cbbRoomID.Items.Add(CStr(Room("room_id")))
        Next

        cbbCustomerID.DropDownStyle = ComboBoxStyle.DropDownList
        lsDataCustomer = oController.findAllCustomer()

        For Each customer In lsDataCustomer
            cbbCustomerID.Items.Add(CStr(customer("customer_id")))
        Next

    End Sub

    'show data in other box when select combo box item in room
    Private Sub cbbRoomID_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbbRoomID.SelectedIndexChanged
        Dim selectedIndex As Integer = cbbRoomID.SelectedIndex
        Dim htData = lsDataRoom.Item(selectedIndex)

        txtRoomNumber.Text = CStr(htData("room_number"))
        txtRoomType.Text = CStr(htData("type"))
        txtRoomPrice.Text = CStr(htData("price"))

    End Sub

    'show data in other box when select combo box item in customer
    Private Sub cbbCustomerID_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbbCustomerID.SelectedIndexChanged
        Dim selectedIndex As Integer = cbbCustomerID.SelectedIndex
        Dim htData = lsDataCustomer.Item(selectedIndex)

        txtCustomerName.Text = CStr(htData("firstname")) & " " & CStr(htData("lastname"))

    End Sub

    'calculate buttom
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If txtRoomPrice.Text IsNot "" And txtNumberDays.Text IsNot "" Then txtTotalPrice.Text = CStr(Convert.ToInt64(txtRoomPrice.Text) * Convert.ToInt64(txtNumberDays.Text))

    End Sub

    Private Function validateFormData() As Boolean
        Dim oValidation As New Validation
        Dim bIsValid As Boolean
        Dim bAllFieldsValid As Boolean
        bAllFieldsValid = True
        Dim tt As New ToolTip()

        ' Check customer not empty
        If txtCustomerName.Text IsNot "" Then
            p1.Visible = False
        Else
            p1.Visible = True
            tt.SetToolTip(p1, "Please choose a customer")
            bAllFieldsValid = False
        End If

        ' Check room not empty
        If txtRoomNumber.Text IsNot "" Then
            p2.Visible = False
        Else
            p2.Visible = True
            tt.SetToolTip(p2, "Please choose a room")
            bAllFieldsValid = False
        End If

        ' Check number of days is number
        bIsValid = IsNumeric(txtNumberDays.Text)
        If bIsValid Then
            p3.Visible = False
        Else
            p3.Visible = True
            tt.SetToolTip(p3, "Number of days must be a number")
            bAllFieldsValid = False
        End If

        ' Check number of guests is number
        bIsValid = IsNumeric(txtNumberGuests.Text)
        If bIsValid Then
            p4.Visible = False
        Else
            p4.Visible = True
            tt.SetToolTip(p4, "Number of guests must be a number")
            bAllFieldsValid = False
        End If

        ' Check total price not empty
        If txtTotalPrice.Text IsNot "" Then
            p6.Visible = False
        Else
            p6.Visible = True
            tt.SetToolTip(p6, "Please calculate the total price")
            bAllFieldsValid = False
        End If

        ' Check comment not empty
        If txtComment.Text IsNot "" Then
            p7.Visible = False
        Else
            p7.Visible = True
            tt.SetToolTip(p7, "Please choose a room")
            bAllFieldsValid = False
        End If

        'Validate all field
        If bAllFieldsValid Then
            Debug.Print("All fields are valid")
        End If
        Return bAllFieldsValid

    End Function


    Private Sub btnAddNewBooking_Click(sender As Object, e As EventArgs) Handles btnAddNewBooking.Click
        Dim bIsValid = validateFormData()
        If bIsValid Then
            ' Instantiate a hashtable and populate it with form data
            Dim htData As Hashtable = New Hashtable

            'take data value from textbox
            htData("bdate") = Today
            htData("num_days") = txtNumberDays.Text
            htData("num_guests") = txtNumberGuests.Text
            htData("checkin_date") = CheckinDate.Value.Date
            htData("comments") = txtComment.Text
            htData("room_id") = cbbRoomID.SelectedItem
            htData("customer_id") = cbbCustomerID.SelectedItem
            htData("total_price") = txtTotalPrice.Text

            Dim oDataController As DataController = New DataController
            Dim iNumRows = oDataController.insertbooking(htData)
            If iNumRows = 1 Then MsgBox("The booking information is recorded.")
        End If
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub FindToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles FindToolStripMenuItem.Click
        FindBooking.Show()
    End Sub

    Private Sub AddToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AddToolStripMenuItem.Click
        Customer.Show()
    End Sub

    Private Sub AddToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles AddToolStripMenuItem1.Click
        Room.Show()
    End Sub

    Private Sub FindToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles FindToolStripMenuItem1.Click
        FindCustomer.Show()
    End Sub

    Private Sub FindToolStripMenuItem2_Click(sender As Object, e As EventArgs) Handles FindToolStripMenuItem2.Click
        FindRoom.Show()
    End Sub

    Private Sub CustomerReportToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CustomerReportToolStripMenuItem.Click
        FindCustomer.Show()
    End Sub
End Class