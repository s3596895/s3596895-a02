﻿Option Explicit On
Option Strict On
Imports System.Data.OleDb
Imports System.IO
'Name: Dao Ngoc Minh
'Description: Class acting As intermediary between the form And database
'Contains most of the CRUD business logic
'Author:Dao Ngoc Minh s3596895
'Date: 22/11/2017
Public Class DataController
    Public Const CONNECTION_STRING As String =
        "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Database.accdb"

    'insert customer data
    Public Function insertCustomer(ByVal htData As Hashtable) As Integer
        'established connection to data source
        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim iNumRows As Integer

        Try
            MsgBox("Connection string: " & oConnection.ConnectionString)

            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            'insert value from customer form to parameter of sql query
            oCommand.CommandText =
              "INSERT INTO Customer (title, firstname, lastname, phone, address, email, dob, gender) VALUES (?, ?, ?, ?, ?, ?, ?, ?);"
            oCommand.Parameters.Add("title", OleDbType.VarChar, 10)
            oCommand.Parameters.Add("firstname", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("lastname", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("phone", OleDbType.BigInt, 255)
            oCommand.Parameters.Add("address", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("email", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("dob", OleDbType.Date, 255)
            oCommand.Parameters.Add("gender", OleDbType.VarChar, 10)

            'take data from form
            oCommand.Parameters("title").Value = CStr(htData("title"))
            oCommand.Parameters("firstname").Value = CStr(htData("firstname"))
            oCommand.Parameters("lastname").Value = CStr(htData("lastname"))
            oCommand.Parameters("phone").Value = CInt(htData("phone"))
            oCommand.Parameters("address").Value = CStr(htData("address"))
            oCommand.Parameters("email").Value = CStr(htData("email"))
            oCommand.Parameters("dob").Value = CDate(htData("dob"))
            oCommand.Parameters("gender").Value = CStr(htData("gender"))

            'execute sql query
            oCommand.Prepare()
            iNumRows = oCommand.ExecuteNonQuery()

            'show error
        Catch ex As Exception
            MsgBox("ERROR: " & ex.Message)
            MsgBox("An error occured. The record wasn't inserted.")
        Finally
            oConnection.Close()
        End Try

        Return iNumRows

    End Function

    'insert room data
    Public Function addroom(ByVal htData As Hashtable) As Integer

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim iNumRows As Integer

        Try
            MsgBox("Connection string: " & oConnection.ConnectionString)
            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            'insert value from room form to parameter of sql query
            oCommand.CommandText =
                "INSERT INTO room (room_number, type, price, num_bed, availability, floor, description) VALUES (?, ?, ?, ?, ?, ?, ?);"
            oCommand.Parameters.Add("room_number", OleDbType.Integer, 11)
            oCommand.Parameters.Add("type", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("price", OleDbType.BigInt, 11)
            oCommand.Parameters.Add("num_bed", OleDbType.Integer, 11)
            oCommand.Parameters.Add("availability", OleDbType.Boolean, 255)
            oCommand.Parameters.Add("floor", OleDbType.Integer, 11)
            oCommand.Parameters.Add("description", OleDbType.VarChar, 255)

            'take data from form
            oCommand.Parameters("room_number").Value = CInt(htData("room_number"))
            oCommand.Parameters("type").Value = CStr(htData("type"))
            oCommand.Parameters("price").Value = CInt(htData("price"))
            oCommand.Parameters("num_bed").Value = CInt(htData("num_bed"))
            oCommand.Parameters("availability").Value = CBool(htData("availability"))
            oCommand.Parameters("floor").Value = CInt(htData("floor"))
            oCommand.Parameters("description").Value = CStr(htData("description"))

            'execute sql query
            oCommand.Prepare()
            iNumRows = oCommand.ExecuteNonQuery()

            'show error
        Catch ex As Exception
            MsgBox("ERROR: " & ex.Message)
            MsgBox("An error occured. The record wasn't inserted.")
        Finally
            oConnection.Close()
        End Try

        Return iNumRows
    End Function

    'insert into booking data
    Public Function insertbooking(ByVal htData As Hashtable) As Integer

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim iNumRows As Integer

        Try
            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            'insert value from booking form to parameter of sql query
            oCommand.CommandText = "INSERT INTO booking (bdate, num_days, num_guests, checkin_date, comments, customer_id, room_id, total_price) VALUES (?, ?, ?, ?, ?, ?, ?, ?);"

            oCommand.Parameters.Add("bdate", OleDbType.Date)
            oCommand.Parameters.Add("num_days", OleDbType.Integer, 12)
            oCommand.Parameters.Add("num_guests", OleDbType.Integer, 12)
            oCommand.Parameters.Add("checkin_date", OleDbType.Date)
            oCommand.Parameters.Add("comments", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("customer_id", OleDbType.Integer, 12)
            oCommand.Parameters.Add("room_id", OleDbType.Integer, 12)
            oCommand.Parameters.Add("total_price", OleDbType.BigInt, 20)
            'take data from form
            oCommand.Parameters("bdate").Value = CStr(htData("bdate"))
            oCommand.Parameters("num_days").Value = CInt(htData("num_days"))
            oCommand.Parameters("num_guests").Value = CInt(htData("num_guests"))
            oCommand.Parameters("checkin_date").Value = CStr(htData("checkin_date"))
            oCommand.Parameters("comments").Value = CStr(htData("comments"))
            oCommand.Parameters("customer_id").Value = CInt(htData("customer_id"))
            oCommand.Parameters("room_id").Value = CInt(htData("room_id"))
            oCommand.Parameters("total_price").Value = CLng(htData("total_price"))
            'execute sql query
            oCommand.Prepare()
            iNumRows = oCommand.ExecuteNonQuery()

            'show error
        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occured. The record wasn't inserted.")
        Finally
            oConnection.Close()
        End Try

        Return iNumRows

    End Function

    'find all data for roomID
    Public Function findAll() As List(Of Hashtable)

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim lsData As New List(Of Hashtable)

        Try
            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            'select all by sql
            oCommand.CommandText =
                    "SELECT * FROM room ORDER BY room_id;"
            oCommand.Prepare()
            Dim oDataReader = oCommand.ExecuteReader()

            'take suitable data from select all in sql
            Dim htTempData As Hashtable
            Do While oDataReader.Read() = True
                htTempData = New Hashtable
                htTempData("room_id") = CStr(oDataReader("room_id"))
                htTempData("room_number") = CStr(oDataReader("room_number"))
                htTempData("type") = CStr(oDataReader("type"))
                htTempData("price") = CStr(oDataReader("price"))
                htTempData("num_bed") = CStr(oDataReader("num_bed"))
                htTempData("availability") = CStr(oDataReader("availability"))
                htTempData("floor") = CStr(oDataReader("floor"))
                htTempData("description") = CStr(oDataReader("description"))
                lsData.Add(htTempData)
            Loop

            'for debug purpose
            Debug.Print("The records were found.")

            'show error
        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The records could not be found!")
        Finally
            oConnection.Close()
        End Try

        Return lsData

    End Function

    'find all customer for customer id in booking form
    Public Function findAllCustomer() As List(Of Hashtable)

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim lsData As New List(Of Hashtable)

        Try
            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            'select all by sql
            oCommand.CommandText =
                    "SELECT * FROM customer ORDER BY customer_id;"
            oCommand.Prepare()
            Dim oDataReader = oCommand.ExecuteReader()

            'take suitable data from select all in sql
            Dim htTempData As Hashtable
            Do While oDataReader.Read() = True
                htTempData = New Hashtable
                htTempData("customer_id") = CStr(oDataReader("customer_id"))
                htTempData("firstname") = CStr(oDataReader("firstname"))
                htTempData("lastname") = CStr(oDataReader("lastname"))
                lsData.Add(htTempData)
            Loop

            'for debug purpose
            Debug.Print("The records were found.")

            'show error
        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The records could not be found!")
        Finally
            oConnection.Close()
        End Try

        Return lsData

    End Function



    'get booking
    Public Function getDataBooking(ByVal keyword As String) As DataTableCollection
        Dim da As OleDbDataAdapter
        Dim ds As DataSet
        Dim tables As DataTableCollection
        Dim source1 As New BindingSource
        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        oConnection.ConnectionString = CONNECTION_STRING
        ds = New DataSet
        da = New OleDbDataAdapter("Select booking.*, customer.*, room.* From room INNER Join (customer INNER Join booking On customer.customer_id = booking.customer_id) ON room.room_id = booking.room_id;", oConnection)
        If keyword <> "" Then da = New OleDbDataAdapter("Select booking.*, customer.*, room.* From room INNER Join (customer INNER Join booking On customer.customer_id = booking.customer_id) ON room.room_id = booking.room_id where (firstname like '" & keyword & "') or (lastname like '" & keyword & "');", oConnection)
        da.Fill(ds, "booking")

        tables = ds.Tables

        Return tables

    End Function

    'get customer
    Public Function getDatacustomer(ByVal keyword As String) As DataTableCollection
        Dim da As OleDbDataAdapter
        Dim ds As DataSet
        Dim tables As DataTableCollection
        Dim source1 As New BindingSource
        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        oConnection.ConnectionString = CONNECTION_STRING
        ds = New DataSet
        da = New OleDbDataAdapter("Select * FROM customer", oConnection)
        'If keyword <> "" And IsNumeric(keyword) = True Then da = New OleDbDataAdapter("Select * FROM customer where customer_id like '" & keyword & "';", oConnection)
        If keyword <> "" Then da = New OleDbDataAdapter("Select * FROM customer where (firstname like '" & keyword & "') or (lastname like '" & keyword & "');", oConnection)
        da.Fill(ds, "booking")

        tables = ds.Tables

        Return tables

    End Function

    'get room
    Public Function getDataRoom(ByVal keyword As String) As DataTableCollection
        Dim da As OleDbDataAdapter
        Dim ds As DataSet
        Dim tables As DataTableCollection
        Dim source1 As New BindingSource
        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        oConnection.ConnectionString = CONNECTION_STRING
        ds = New DataSet
        da = New OleDbDataAdapter("Select * FROM room", oConnection)
        'If keyword <> "" And IsNumeric(keyword) = True Then da = New OleDbDataAdapter("Select * FROM customer where customer_id like " & keyword & ";", oConnection)
        If keyword <> "" Then da = New OleDbDataAdapter("Select * FROM room where (type like '" & keyword & "');", oConnection)
        da.Fill(ds, "room")

        tables = ds.Tables

        Return tables

    End Function


    Public Function updatebooking(ByVal htData As Hashtable) As Integer
        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim iNumRows As Integer
        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)
            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            oCommand.CommandText =
                "UPDATE booking SET checkin_date = ?,comments = ?, num_days = ?, num_guests = ? WHERE booking_id = ?;"

            oCommand.Parameters.Add("checkin_date", OleDbType.Date)
            oCommand.Parameters.Add("comments", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("num_days", OleDbType.Integer, 12)
            oCommand.Parameters.Add("num_guests", OleDbType.Integer, 12)
            oCommand.Parameters.Add("booking_id", OleDbType.Integer, 12)

            oCommand.Parameters("checkin_date").Value = CDate(htData("checkin_date"))
            oCommand.Parameters("comments").Value = CStr(htData("comments"))
            oCommand.Parameters("num_days").Value = CInt(htData("num_days"))
            oCommand.Parameters("num_guests").Value = CInt(htData("num_guests"))
            oCommand.Parameters("booking_id").Value = CInt(htData("booking_id"))

            oCommand.Prepare()
            iNumRows = oCommand.ExecuteNonQuery()

            Debug.Print(CStr(iNumRows))

            Debug.Print("The record was updated.")
        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
        Finally
            oConnection.Close()
        End Try
        Return iNumRows
    End Function



    Public Function updatecustomer(ByVal htData As Hashtable) As Integer
        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim iNumRows As Integer
        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)
            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            oCommand.CommandText =
                "UPDATE customer SET title = ?, firstname = ?, lastname = ?, phone = ?, address = ?, email = ?, dob = ?, gender = ? WHERE customer_id = ?;"

            oCommand.Parameters.Add("title", OleDbType.VarChar, 10)
            oCommand.Parameters.Add("firstname", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("lastname", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("phone", OleDbType.BigInt, 255)
            oCommand.Parameters.Add("address", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("email", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("dob", OleDbType.Date, 255)
            oCommand.Parameters.Add("gender", OleDbType.VarChar, 10)
            oCommand.Parameters.Add("customer_id", OleDbType.Integer, 12)

            'take data from form
            oCommand.Parameters("title").Value = CStr(htData("title"))
            oCommand.Parameters("firstname").Value = CStr(htData("firstname"))
            oCommand.Parameters("lastname").Value = CStr(htData("lastname"))
            oCommand.Parameters("phone").Value = CInt(htData("phone"))
            oCommand.Parameters("address").Value = CStr(htData("address"))
            oCommand.Parameters("email").Value = CStr(htData("email"))
            oCommand.Parameters("dob").Value = CDate(htData("dob"))
            oCommand.Parameters("gender").Value = CStr(htData("gender"))
            oCommand.Parameters("customer_id").Value = CInt(htData("customer_id"))

            oCommand.Prepare()
            iNumRows = oCommand.ExecuteNonQuery()

            Debug.Print(CStr(iNumRows))

            Debug.Print("The record was updated.")
        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
        Finally
            oConnection.Close()
        End Try
        Return iNumRows
    End Function

    'update room
    Public Function updateroom(ByVal htData As Hashtable) As Integer
        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim iNumRows As Integer
        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)
            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            oCommand.CommandText =
                "UPDATE room SET room_number = ?, type = ?, price = ?, num_bed = ?, availability = ?, floor = ?, description = ? WHERE room_id = ?;"

            oCommand.Parameters.Add("room_number", OleDbType.Integer, 11)
            oCommand.Parameters.Add("type", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("price", OleDbType.BigInt, 11)
            oCommand.Parameters.Add("num_bed", OleDbType.Integer, 11)
            oCommand.Parameters.Add("availability", OleDbType.Boolean, 255)
            oCommand.Parameters.Add("floor", OleDbType.Integer, 11)
            oCommand.Parameters.Add("description", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("room_id", OleDbType.Integer, 12)

            'take data from form
            oCommand.Parameters("room_number").Value = CInt(htData("room_number"))
            oCommand.Parameters("type").Value = CStr(htData("type"))
            oCommand.Parameters("price").Value = CInt(htData("price"))
            oCommand.Parameters("num_bed").Value = CInt(htData("num_bed"))
            oCommand.Parameters("availability").Value = CBool(htData("availability"))
            oCommand.Parameters("floor").Value = CInt(htData("floor"))
            oCommand.Parameters("description").Value = CStr(htData("description"))
            oCommand.Parameters("room_id").Value = CInt(htData("room_id"))

            oCommand.Prepare()
            iNumRows = oCommand.ExecuteNonQuery()

            Debug.Print(CStr(iNumRows))

            Debug.Print("The record was updated.")
        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
        Finally
            oConnection.Close()
        End Try
        Return iNumRows
    End Function

    'delete booking
    Public Function deleteBooking(ByVal deletevar As String) As Integer
        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim iNumRows As Integer

        If MsgBox("Are you sure?", MsgBoxStyle.YesNo, Title:="confirm") = vbYes Then
            Try
                Debug.Print("Connection string: " & oConnection.ConnectionString)

                oConnection.Open()
                Dim oCommand As OleDbCommand = New OleDbCommand
                oCommand.Connection = oConnection

                'TODO    
                oCommand.CommandText = "DELETE FROM booking WHERE booking_id = ?;"
                oCommand.Parameters.Add("booking_id", OleDbType.Integer, 12)
                oCommand.Parameters("booking_id").Value = CInt(deletevar)
                oCommand.Prepare()
                iNumRows = oCommand.ExecuteNonQuery()

                Debug.Print(CStr(iNumRows))
                Debug.Print("The record was deleted.")
            Catch ex As Exception
                Debug.Print("ERROR: " & ex.Message)
            Finally
                oConnection.Close()
            End Try
        Else
        End If
        Return iNumRows

    End Function

    'This function delete 1 customer record in findcustomer form
    Public Function deletecustomer(ByVal deletevar As String) As Integer
        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim iNumRows As Integer

        If MsgBox("Are you sure?", MsgBoxStyle.YesNo, Title:="confirm") = vbYes Then
            Try
                Debug.Print("Connection string: " & oConnection.ConnectionString)

                oConnection.Open()
                Dim oCommand As OleDbCommand = New OleDbCommand
                oCommand.Connection = oConnection

                'TODO    
                oCommand.CommandText = "DELETE FROM customer WHERE customer_id = ?;"
                oCommand.Parameters.Add("customer_id", OleDbType.Integer, 12)
                oCommand.Parameters("customer_id").Value = CInt(deletevar)
                oCommand.Prepare()
                iNumRows = oCommand.ExecuteNonQuery()

                Debug.Print(CStr(iNumRows))
                Debug.Print("The record was deleted.")
            Catch ex As Exception
                Debug.Print("ERROR: " & ex.Message)
            Finally
                oConnection.Close()
            End Try
        Else
        End If

        Return iNumRows
    End Function

    'delete room
    Public Function deleteroom(ByVal deletevar As String) As Integer
        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim iNumRows As Integer

        If MsgBox("Are you sure?", MsgBoxStyle.YesNo, Title:="confirm") = vbYes Then
            Try
                Debug.Print("Connection string: " & oConnection.ConnectionString)

                oConnection.Open()
                Dim oCommand As OleDbCommand = New OleDbCommand
                oCommand.Connection = oConnection

                'TODO    
                oCommand.CommandText = "DELETE FROM room WHERE room_id = ?;"
                oCommand.Parameters.Add("room_id", OleDbType.Integer, 12)
                oCommand.Parameters("room_id").Value = CInt(deletevar)
                oCommand.Prepare()
                iNumRows = oCommand.ExecuteNonQuery()

                Debug.Print(CStr(iNumRows))
                Debug.Print("The record was deleted.")
            Catch ex As Exception
                Debug.Print("ERROR: " & ex.Message)
            Finally
                oConnection.Close()
            End Try
        Else
        End If

        Return iNumRows
    End Function

    'find
    Public Function findAllBooking() As List(Of Hashtable)
        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim lsData As New List(Of Hashtable)

        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)

            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            oCommand.CommandText =
                "SELECT * FROM booking ORDER BY booking_id;"

            oCommand.Prepare()

            Dim oDataReader = oCommand.ExecuteReader()

            Dim htTempData As Hashtable
            Do While oDataReader.Read() = True
                htTempData = New Hashtable
                htTempData("booking_id") = CStr(oDataReader("booking_id"))
                htTempData("bdate") = CDate(oDataReader("bdate"))
                htTempData("num_days") = CInt(oDataReader("num_days"))
                htTempData("num_guests") = CInt(oDataReader("num_guests"))
                htTempData("checkin_date") = CDate(oDataReader("checkin_date"))
                htTempData("comments") = CStr(oDataReader("comments"))
                htTempData("customer_id") = CInt(oDataReader("customer_id"))
                htTempData("room_id") = CInt(oDataReader("room_id"))
                htTempData("total_price") = CLng(oDataReader("total_price"))
                lsData.Add(htTempData)
            Loop
            Debug.Print("The record was found.")
        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The record(s) could not be found!")
        Finally
            oConnection.Close()
        End Try
        Return lsData
    End Function

    Private Sub saveReport(ByVal sReportContent As String, ByVal sReportFilename As String)
        Debug.Print("saveReport: " & sReportFilename)

        'This statement opens a file for writing purposes in the folder where the ".exe file" was loaded (this is the "Debug folder").
        'The name of the file is as specificed by variable "sReportFilename".
        '"StreamWriter class" is in the System.IO namespace.
        Dim oReportFile As StreamWriter = New StreamWriter(Application.StartupPath & "\" & sReportFilename)

        'Check if the file is opened before starting to write to it.
        'If it was opened, then the string containing the report is written in a single step to the file and the file is closed.
        If Not (oReportFile Is Nothing) Then
            oReportFile.Write(sReportContent)
            oReportFile.Close()
        End If
    End Sub

    Public Sub createBreakReport()
        'To print information to let you know that you are in a certain function.
        Debug.Print("CreateBreakReport ...")

        '"findAll()" method to find all the records in the product table before. 
        Dim lsData = findAllBooking()

        'Declares it as a variable due to the report title method will be called for flexible and reusability reasons.
        Dim sReportTitle = "Reservation Control Break Report"

        'A call made to "generateReport01()" to create "HTML file". 
        'Parameter passed to this method are the list of the hashtables and the title of the report. 
        'This method will returns a single string which contains the whole "HTML file".
        Dim sReportContent = generateBreakReport(lsData, sReportTitle)

        'Declares it as a variable to avoide hard-coding the name of "HTML file" will be stored on disk.
        Dim sReportFilename = "ReservationBreakReport.html"

        '"saveReport()" method will save the "HTML file" on disk (in the "Debug" folder). 
        saveReport(sReportContent, sReportFilename)

        'Load the generated file in your default browser. 
        'To print the path to the file as frustrating experience issues with double quotes (it is used for escaping double quotes).
        'Like sParam: "C:\Users\...\Task-06.03\bin\Debug\ReservationReport-01.html".
        Dim sParam As String = """" & Application.StartupPath & "\" & sReportFilename & """"
        Debug.Print("sParam: " & sParam)
        System.Diagnostics.Process.Start(sParam)

    End Sub

    Private Function generateBreakReport(ByVal lsData As List(Of Hashtable), ByVal sReportTitle As String) As String
        Debug.Print("GenerateBreakReport ...")

        Dim sReportContent As String

        'TODO
        '1. Generate the start of the "HTML file"
        Dim sDoctype As String = "<!DOCTYPE html>"
        Dim sHtmlStartTag As String = "<html lang=""en"">"
        Dim sHeadTitle As String = "<head><title>" & sReportTitle & "</title></head>"
        Dim sBodyStartTag As String = "<body>"
        Dim sReportHeading As String = "<h1>" & sReportTitle & "</h1>"
        sReportContent = sDoctype & vbCrLf & sHtmlStartTag & vbCrLf & vbCrLf & sHeadTitle & vbCrLf & sBodyStartTag & vbCrLf & sReportHeading & vbCrLf

        '2. Generate the reservation table and its rows.
        Dim sTable = generateControlBreakTable(lsData)
        'TODO - construct the rows of the table
        sReportContent &= sTable & vbCrLf

        '3. Generate the end of the "HTML file"
        Dim sBodyEndTag As String = "</body>"
        Dim sHTMLEndTag As String = "</html>"
        sReportContent &= sBodyEndTag & vbCrLf & sHTMLEndTag

        Return sReportContent

    End Function

    Private Function generateControlBreakTable(ByVal lsData As List(Of Hashtable)) As String
        'Generate the start of the table
        Dim sTable = "<table border=""1"">" & vbCrLf
        Dim htSample As Hashtable = lsData.Item(0)
        'Dim lsKeys = htSample.Keys

        Dim lsKeys As List(Of String) = New List(Of String)
        lsKeys.Add("booking_id")
        lsKeys.Add("room_id")
        lsKeys.Add("customer_id")
        lsKeys.Add("num_days")
        lsKeys.Add("num_guests")
        lsKeys.Add("checkin_date")
        lsKeys.Add("total_price")
        lsKeys.Add("comments")

        'Generate the header row
        Dim sHeaderRow = "<tr>" & vbCrLf
        For Each key In lsKeys
            sHeaderRow &= "<th>" & CStr(key) & "</th>" & vbCrLf
        Next
        sHeaderRow &= "</tr>" & vbCrLf
        Debug.Print("sHeaderRow: " & sHeaderRow)
        sTable &= sHeaderRow

        'Generate the table rows
        sTable &= generateTableRows(lsData, lsKeys)

        'Generate the end of the table
        sTable &= "</table>" & vbCrLf

        Return sTable
    End Function

    Private Function generateTableRows(ByVal lsData As List(Of Hashtable), ByVal lsKeys As List(Of String)) As String

        '1. Initialisation 
        Dim sRows As String = ""
        Dim sTableRow As String
        Dim iCountRecordsPerDate As Integer = 0
        Dim bAvailability As Boolean = True
        Dim sCurrentControlField As String = ""
        Dim sPreviousControlField As String = ""

        '2. Loop through the list of hashtables
        For Each record In lsData

            '2.1. Get a room and set the current key
            Dim room As Hashtable = record
            sCurrentControlField = CStr(room("availability"))

            '2.2. Do not check for control break on the first iteration of the loop

            If bAvailability Then
                bAvailability = False
            Else
                'Output total row if change in control field
                'And reset the total
                If sCurrentControlField <> sPreviousControlField Then
                    sTableRow = "<tr><td colspan = """ & lsKeys.Count & """>" & " Total booked rooms in " & sPreviousControlField & " Date: " & iCountRecordsPerDate & "</td></tr>" & vbCrLf
                    sRows &= sTableRow
                    iCountRecordsPerDate = 0
                End If
            End If

            '2.3. Output a normal row for every pass through the list
            sTableRow = "<tr>" & vbCrLf
            For Each key In lsKeys
                sTableRow &= "<td>" & CStr(room(key)) & "</td>" & vbCrLf
            Next
            sTableRow &= "</tr>" & vbCrLf
            Debug.Print("sTableRow: " & sTableRow)
            sRows &= sTableRow

            '2.4. Update control field and increment total
            sPreviousControlField = sCurrentControlField
            iCountRecordsPerDate += 1
        Next

        '3. After the loop, need to output the last total row
        sTableRow = "<tr><td colspan = """ & lsKeys.Count & """>" & " Total booked rooms in " & sPreviousControlField & " Date: " & iCountRecordsPerDate & "</td></tr>" & vbCrLf
        sRows &= sTableRow

        Return sRows

    End Function


End Class
